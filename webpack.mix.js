const mix = require('laravel-mix');

mix.browserSync({
    proxy: false,
    port: '3000',
    server: {
        baseDir: './'
    },
    files: [
        'assets/dist/css/**.css',
        'assets/dist/js/**.js',
        'asset/dist/images/*',
        '*.html',
    ]
});

mix.sass('assets/src/scss/style.scss', 'assets/dist/css')
    .options({
        processCssUrls: false
    })
    .copy('assets/src/images', 'assets/dist/images');
